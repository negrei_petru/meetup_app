class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string :name
      t.string :age
      t.string :avatar_file_name

      t.timestamps
    end
  end
end
