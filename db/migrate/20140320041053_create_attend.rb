class CreateAttend < ActiveRecord::Migration
  def change
    create_table :attends do |t|
      t.integer :user_id
      t.integer :meetup_id
    end

     add_index(:attends, [:user_id, :meetup_id])
  end

  def down
    drop_idnex :attends
  end
end
