class AttendsController < ApplicationController
  before_filter :current_user
  before_filter :authenticate!

  def create
    if current_user.attend_for_meetup(meetup)
      message = "You're are attending this meetup"
    else
      message = "You are already attending this meetup"
    end

    flash[:success] = message
    redirect_to :back
  end

  def destroy
    if current_user.cancel_attend_for_meetup(meetup)
      message = "You're successfully canceled you attendence"
    else
      message = "You didn't attend this meetup"
    end

    flash[:success] = message

    redirect_to :back
  end

  private

  def meetup
    @meetup ||= Meetup.find params[:meetup_id]
  end
end