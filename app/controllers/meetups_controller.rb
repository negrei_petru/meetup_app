class MeetupsController < ApplicationController
  authorize_resource only: [:new, :show, :edit, :update, :destroy]
  before_filter :meetup, only: [:show, :edit, :update, :destroy]
  helper_method :meetup

  def index
    @meetups = Meetup.all
  end

  def new
    @meetup = Meetup.new
  end

  def create
    @meetup = Meetup.new meetup_params
    if @meetup.save
      flash[:succes] = "Successfully created a meetup"
      redirect_to meetups_path
    else
      flash[:error] = "Couldn't create a meetup"
      render :new 
    end
  end

  def show
  end

  def edit
  end

  def update  
    if @meetup.update_attributes(meetup_params)
      flash[:succes] = "Update successfully a meetup"
      redirect_to meetups_path
    else
      flash[:error] = "Couldn't update a meetup"
      render :new 
    end
  end

  def destroy
    @meetup.destroy
    flash[:succes] = "Successfully deleted a meetup"
    redirect_to meetups_path
  end

  def meetup_params
    params.require(:meetup).permit(:name, :description, :address, :city, :state, :zip, :start_at, :ends_at)
  end

  private

  def meetup
    @meetup ||= Meetup.find params[:id]
  end
end