class UsersController < ApplicationController
  load_and_authorize_resource only: [:edit, :update, :destroy]
  before_filter :authenticate!, only: [:edit, :update, :destroy]
  
  def index
    @users = User.includes(:profile).page(params[:page])
  end

  def new
    @user = User.new
    @profile = Profile.new
  end

  def create
    @user = User.new user_params
    @profile = Profile.new profile_params

    if @user.valid? && @profile.valid?
      @user.profile = @profile 
      @user.save

      user_session.start_session @user 
      flash[:success] = "Acount created and logged in successfully"
      redirect_to root_url
    else
      @profile.valid?
      flash[:error] = "There was an problem."
      render :new
    end
  end


  def edit
    @profile = @user.profile || Profile.new
  end

  def update
    @user.build_profile 
    @user.assign_attributes user_params
    @user.profile.assign_attributes profile_params

    if @user.valid? && @user.profile.valid?
      @user.save
      flash[:success] = "Updated the profile succses"
      redirect_to users_path
    else
      @user.profile.valid?
      @profile = @user.profile

      flash[:error] = "There was a problem"
      redirect_to 
      render :edit
    end
  end

  def destroy
    logout = current_user.id == user.id
    @user.destroy
    user_session.destroy_session if logout


    flash[:success] = "The account has been deleted"
    redirect_to users_path
  end

  private

  def user
    @user ||= User.find params[:id]
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :profile)
  end

  def profile_params
    params.require(:profile).permit(:name, :age, :avatar_file_name)
  end

end