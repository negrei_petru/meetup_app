class User < ActiveRecord::Base
  before_save :hash_password

  validates :email, presence: true, uniqueness: true
  validates :password, confirmation: true, presence: {on: :create}

  has_and_belongs_to_many :meetups, join_table: 'attends'
  has_one :profile, dependent: :destroy

  delegate :name, to: :profile, allow_nil: true
  delegate :age, to: :profile, allow_nil: true

  def hash_password
    if password_changed?
      write_attribute(:password, BCrypt::Password.create(password))
    end
  end

  def attend_for_meetup(meetup)
    meetups << meetup unless attends_for_meetup?(meetup)
  end

  def cancel_attend_for_meetup(meetup)
    meetups.delete meetup if attends_for_meetup?(meetup)
  end

  def attends_for_meetup?(meetup)
    meetups.include? meetup
  end

end
