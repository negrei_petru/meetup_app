class Profile < ActiveRecord::Base
  belongs_to :user

  has_attached_file :avatar, default_url: '/assets/default-avatar.jpg'
 # validates_attachment_content_type :avatar, :content_type => ["avatar/jpg", "avatar/jpeg", "avatar/png"]
  do_not_validate_attachment_file_type :avatar

  validates :name, presence:true
  validates :age, numericality: { only_integer: true}, allow_nil: true
end
