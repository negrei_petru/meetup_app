In this project we will build a meetup rsvp application.

## Introduction

This application is development only with ruby on rails and the main purpose is to 
posts information about a meeting and permit users to sign for this meeting.

## Description

### Users

As team leader, I can:

* Browse all meetings
* Browse all users (for admin)
* Attend for this meeting
* Add/Delete/show a meeting.

Below is represented the main page where the user is presented with the most recent meetups and announcements.

![Static_Main_Page](https://bitbucket.org/negrei_petru/meetup_app/raw/master/screenshots/static_main_page.png)

![Static_Main_Page](https://bitbucket.org/negrei_petru/meetup_app/raw/master/screenshots/user_page.png)

![Static_Main_Page](https://bitbucket.org/negrei_petru/meetup_app/raw/master/screenshots/user_edit_page.png)

### Meetups

Then the user login in the system and he can see all available meetings and can choose to attend them.

![Main_Page](https://bitbucket.org/negrei_petru/meetup_app/raw/master/screenshots/main_page.png)

Here is more detailed view of a meeting

![Main_Page](https://bitbucket.org/negrei_petru/meetup_app/raw/master/screenshots/meeting.png)